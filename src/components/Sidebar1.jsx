import React from "react";
import category_icon from "./Images/category_icon.png"
import cube from "./Images/cube.png";
import list from "./Images/list.png";
import messenger from "./Images/messenger.png";
import success from "./Images/success.png";
import security from "./Images/security.png";
import user from "./Images/users.png";
import lachlan from "./Images/lachlan.jpg";
import raamin from "./Images/raamin.jpg";
import noname from "./Images/nonamesontheway.jpg";
import plus from "./Images/plus.png";

function Sidebar1() {
    return (
        <div class="sticky top-0">
            {/* <!-- This is an example component --> */}
            <div class="bg-gray-400 w-20 h-screen  ">
                {/* <!-- Sidebar --> */}
                <nav class="flex flex-col  ">
                    <div class="">
                        <div>
                            <ul>
                                <li class="mb-15">
                                    <div>
                                        <img class="h-5 w-5 m-auto" src={category_icon} />
                                    </div>
                                </li>
                                <div class="mb-5 mt-10">
                                    <li>
                                        <div class="mt-10 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={cube} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={list} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={messenger} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={list} />
                                        </div>
                                    </li>
                                </div>

                                <div class="mb-5 mt-10">
                                    <li>
                                        <div class="mt-10 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={success} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={security} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto" src={user} />
                                        </div>
                                    </li>
                                </div>


                                <div class="mb-5 mt-10">
                                    <li>
                                        <div class="mt-10 mb-2">
                                            <img class="h-5 w-5 m-auto rounded-full" src={lachlan} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto rounded-full" src={raamin} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto rounded-full" src={noname} />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mt-3 mb-2 ">
                                            <img class="h-5 w-5 m-auto rounded-full" src={plus} />
                                        </div>
                                    </li>
                                </div>
                            </ul>
                        </div>
                    </div>
                </nav>

            </div>

        </div>
    )
}
export default Sidebar1;