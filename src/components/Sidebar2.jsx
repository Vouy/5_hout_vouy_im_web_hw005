import React from "react";
import lachlan from "./Images/lachlan.jpg";
import raamin from "./Images/raamin.jpg";
import noname from "./Images/nonamesontheway.jpg";
import christina from "./Images/christina.jpg";
import notification  from "./Images/notification.png";
import comment from "./Images/comment.png";

function Sidebar2() {
    return (
        <div class="sticky top-0">
            
            <div class="relative grid h-screen w-full max-w-[28rem] flex-col items-end justify-center overflow-hidden rounded-xl bg-white bg-clip-border text-center text-gray-700">
                <div class="absolute inset-0 m-0 h-full w-full overflow-hidden rounded-none bg-transparent bg-camping bg-cover bg-clip-border bg-center text-gray-700 shadow-none">
                    <div class="to-bg-black-10 absolute inset-0 h-full w-full bg-gradient-to-t from-black/80 via-black/50 "></div>
                    <div class="float-right  mx-10">
                    <img
                        src={notification}
                        class=" inline-block h-[20px] w-[20px]  object-cover object-center mt-10 mx-1"
                    />
                     <img
                        src={comment}
                        class=" inline-block h-[20px] w-[20px]  object-cover object-center mt-10 mx-1"
                    />
                     <img
                        src={raamin}
                        class=" inline-block h-[30px] w-[30px] rounded-full border-2 border-white object-cover object-center mt-10 mx-1"
                    />
                    </div>
                    
                    <button class=" float-right mx-10 mt-5 bg-yellow-500 text-white font-bold py-2 px-4 w-[170px] rounded">My Amazing Trip</button>
                </div>
                <div class="relative p-6 py-[100px] px-6 md:px-12">
                    <h2 class="mb-6  block font-sans text-3xl font-medium  tracking-normal text-white antialiased">
                    I like laying down on the sand and looking at the moon.
                    </h2>
                    <p class="text-white mb-5"> 26 People going to this trip </p>
                    <img
                        src={lachlan}
                        class="relative inline-block h-[35px] w-[35px] rounded-full border-2 border-white object-cover object-center mx-2"
                    />
                     <img
                        src={raamin}
                        class="relative inline-block h-[35px] w-[35px] rounded-full border-2 border-white object-cover object-center mx-2"
                    />
                    <img
                        src={noname}
                        class="relative inline-block h-[35px] w-[35px] rounded-full border-2 border-white object-cover object-center mx-2"
                    />
                    <div class="relative inline-block h-[35px] w-[35px] rounded-full border-2 border-white  mx-2 bg-white">
                        23+
                    </div>
                
                </div>
            </div>

        </div>
    )
}
export default Sidebar2;