import React, { useEffect, useState } from "react";
import Card from "./Card";
import InputDetail from "./InputDetail";

function Contain({ data, setData }) {
    // console.log(data);
    // const [newStatus,setNewStatus] = useState("");
    function onButtonChange(id) {
        data.map(data => {
            if (data.id == id) {
                if (data.status == "beach") {
                    data.status = "mountain";
                } else if (data.status == "mountain") {
                    data.status = "forest";
                } else if (data.status == "forest") {
                    data.status = "beach";
                }
            }
        })
        setData([...data])
    }
    // useEffect(()=>{
    //     console.log("staus: ",status2);
    //     setStatus2(true)
    // })
    return (
        <div>
            {/* {setStatus2(true)} */}
            <div class="float-left">
                <h1 class="font-bold my-8">Good Evening Team!</h1>
            </div>
            {/* Pop Up */}
            <div class="mr-10 ">
                {/* <!-- The button to open modal --> */}
              
                <label  for="my-modal-3" class="float-right btn my-8" >ADD NEW TRIP</label>
                    <input type="checkbox" id="my-modal-3" class="modal-toggle" />
                    <div class="modal ">
                        <div class="modal-box relative">
                            <label for="my-modal-3" class="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                            <InputDetail datas={data} setDatas={setData}/>
                        </div>
                    </div>
                </div>
            {/* <!-- Put this part before  */}

            {/* Card */}
            <div class="clear-both">
                <Card datas={data} onButtonChange={onButtonChange} />
            </div>
        </div>
    )
}
export default Contain;