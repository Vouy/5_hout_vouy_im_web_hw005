import React, { useState } from "react";

function Card({ datas, onButtonChange }) {
    const [showMore, setShowMore] = useState([])

    return (
        <div class="grid grid-cols-3 gap-3 m-5 ml-3">
            {datas.map((item) => (
                <div class="overflow-y-auto">
                <div
                    key={item.id}
                    class="block max-w-sm rounded-lg bg-cyan-900 p-6 shadow-lg h-[300px]">
                    <h5
                        class="mb-2 text-xl font-medium leading-tight font-oswal text-white">
                        {item.title}
                    </h5>
                    <p class="mb-4 text-base  text-white line-clamp-3">
                        {item.description}
                    </p>
                    <h1 class=" text-white mr-[200px] w-[100px] ">People Going</h1>
                    <p class="mb-4 text-white mr-[200px] text-xl">
                        {item.peopleGoing}
                    </p>

                    <div class="flex">
                        <button
                            type="button"
                            class={`${item.status === "beach" ? `capitalize rounded w-[100px] mx-2 bg-blue-600` : item.status === "mountain" ? `  capitalize rounded w-[100px] mx-2  bg-gray-200` : ` capitalize rounded w-[100px] mx-2  bg-green-600`}`}
                            data-te-ripple-init
                            data-te-ripple-color="light" onClick={(e) => {
                                e.preventDefault()
                                onButtonChange(item.id)
                            }}>
                            {item.status}
                        </button>
                        <div>
                            <label for="my-modal" class="btn"
                                onClick={() => setShowMore(item)}>Read Details</label>
                            <input type="checkbox" id="my-modal" class="modal-toggle" />
                            <div class="modal">
                                <div class="modal-box">
                                    <div class="modal-action">
                                        {showMore.title}<br />
                                        {showMore.description}<br />
                                        Around {showMore.peopleGoing} go there<br />
                                        <label for="my-modal" class="btn">Exit</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
            ))}

        </div>

    )
}
export default Card;