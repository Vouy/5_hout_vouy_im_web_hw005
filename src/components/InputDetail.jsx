import React, { useState } from "react";
import Card from "./Card";
const InputDetail = ({ datas, setDatas,setStatus2 }) => {
    const [newData, setNewData] = useState([]);
  
    const inputHandler = (e) => {
        // console.log(e.target.name);
        setNewData({ ...newData, [e.target.name]: e.target.value, })
    }

    const submitHandler = (e) => {
        console.log(e);
        e.preventDefault();
        setDatas([...datas, { id: datas.length + 1, ...newData }])
        e.target.reset()
    }
    return (
        <div>
            <form onSubmit={submitHandler}>
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900">Title</label>
                    <input type="text" name="title" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Title" required onChange={inputHandler} />
                </div>
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900 ">Description</label>
                    <input type="text" name="description" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Description" required onChange={inputHandler} />
                </div>
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900 ">People going</label>
                    <input type="number" name="peopleGoing" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="3000" required onChange={inputHandler} />
                </div>
                <div class="mb-6">
                    <label class="block mb-2 text-base font-medium text-gray-900 ">Type of Adventure</label>
                    <select  name="status" id="large" class="block w-full px-4 py-3 text-base text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500" onChange={inputHandler} >
                        <option disabled  selected >---- Choose any option -----</option>
                        <option value="beach">Beach</option>
                        <option value="forest">Forest</option>
                        <option value="mountain">Mountain</option>
                    </select>
                </div>
                    <button  type="submit">
                        <label for="my-modal-3" className="btn">Submit</label>
                    </button>
            </form>
        </div>
    )
}
export default InputDetail;