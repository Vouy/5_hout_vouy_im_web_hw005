/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    fontFamily:{
        'oswal' :  'Oswald, sans-serif;'
    },

    extend: {
      backgroundImage: {
        'camping': "url('./components/Images/camping.jpg')",
      }
    },
  },
  plugins: [require("daisyui"),require('@tailwindcss/line-clamp')],


}
